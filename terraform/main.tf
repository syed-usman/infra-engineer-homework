terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/52496496/terraform/state/default"
    lock_address   = "https://gitlab.com/api/v4/projects/52496496/terraform/state/default/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/52496496/terraform/state/default/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.28.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "16.6.0"
    }
  }
}

# AWS Provider Configuration
provider "aws" {
  region = var.aws_region # Define the AWS region for resource deployment
}


# Add a project owned by the user
provider "gitlab" {
  token = var.gitlab_access_token
}

data "gitlab_project" "infra-engineer-homework" {
  id = 52496496
}


# Amazon ECS Cluster
# Defines an ECS cluster to manage containerized applications
resource "aws_ecs_cluster" "streamlit_cluster" {
  name = "alcemy-streamlit-cluster" # Name of the ECS cluster
}

# Amazon ECS Task Definition
# Defines a task that runs the Docker container in the ECS cluster
resource "aws_ecs_task_definition" "alcemy_streamlit_task" {
  family                   = "alcemy-streamlit-task"
  network_mode             = "awsvpc"                              # Network mode for the task
  requires_compatibilities = ["FARGATE"]                           # Compatibility with Fargate launch type
  cpu                      = "256"                                 # CPU units for the task
  memory                   = "512"                                 # Memory allocation for the task
  execution_role_arn       = aws_iam_role.ecsTaskExecutionRole.arn # IAM role for task execution

  # Container definition including image URL, CPU, memory, and logging configuration
  container_definitions = jsonencode([
    {
      name      = "alcemy-streamlit-container"
      image     = var.ecr_repo_uri # Docker image URL from ECR
      essential = true
      cpu       = 256
      memory    = 512
      portMappings = [
        {
          containerPort = 8501 # Port the container will listen on
          hostPort      = 8501
        }
      ]
      logConfiguration = {
        logDriver = "awslogs" # Defines the logging driver (AWS CloudWatch Logs)
        options = {
          awslogs-group         = "/ecs/alcemy-streamlit-task"
          awslogs-region        = var.aws_region
          awslogs-create-group  = "true"
          awslogs-stream-prefix = "ecs"
        }
      }
    }
  ])
}

# IAM Role for ECS Task Execution
# Defines an IAM role that allows ECS tasks to interact with other AWS services
resource "aws_iam_role" "ecsTaskExecutionRole" {
  name               = "ecsTaskExecutionRole"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json # References the policy document below
}

# IAM Policy Document for ECS Task Execution Role
# Specifies the policy that allows the ECS tasks to assume the role
data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"] # Service that can assume this role
    }
  }
}

resource "aws_iam_policy" "ecs_logging_policy" {
  name        = "ecsLoggingPolicy"
  description = "A policy that allows ECS tasks to create and manage log groups and streams in CloudWatch Logs."

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ],
        Effect   = "Allow",
        Resource = "arn:aws:logs:*:*:*"
      }
    ]
  })
}

# IAM Role Policy Attachment

resource "aws_iam_role_policy_attachment" "ecs_logging_policy_attachment" {
  role       = aws_iam_role.ecsTaskExecutionRole.name
  policy_arn = aws_iam_policy.ecs_logging_policy.arn
}

# Attaches the AmazonECSTaskExecutionRolePolicy to the ECS task execution role
resource "aws_iam_role_policy_attachment" "ecsTaskExecutionRole_policy" {
  role       = aws_iam_role.ecsTaskExecutionRole.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

# Default VPC
# Retrieves the default VPC for the AWS account
resource "aws_default_vpc" "default_vpc" {
}

# Default Subnets
# Retrieves the default subnets for the AWS account in specified availability zones
resource "aws_default_subnet" "default_subnet_a" {
  availability_zone = var.availability_zone_a # AZ for the first default subnet
}

resource "aws_default_subnet" "default_subnet_b" {
  availability_zone = var.availability_zone_b # AZ for the second default subnet
}

# Application Load Balancer (ALB)
# Creates an ALB for distributing incoming application traffic
resource "aws_alb" "application_load_balancer" {
  name               = "alecmy-load-balancer-dev" # Name of the ALB
  load_balancer_type = "application"
  subnets = [ # List of subnet IDs for the ALB
    aws_default_subnet.default_subnet_a.id,
    aws_default_subnet.default_subnet_b.id
  ]
  security_groups = [aws_security_group.load_balancer_security_group.id] # Security group for the ALB
}

# Security Group for the Load Balancer
# Defines a security group with ingress rules for the ALB
resource "aws_security_group" "load_balancer_security_group" {
  ingress {
    from_port   = 80 # Allow inbound traffic on port 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Allow traffic from any source
  }

  egress {
    from_port   = 0 # Allow all outbound traffic
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Target Group for the Load Balancer
# Defines a target group for routing requests to registered targets
resource "aws_lb_target_group" "target_group" {
  name        = "target-group"
  port        = 80 # Port for routing requests
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = aws_default_vpc.default_vpc.id # VPC for the target group
}

# Listener for the Load Balancer
# Configures a listener for the ALB to forward HTTP requests to the target group
resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_alb.application_load_balancer.arn # ALB ARN
  port              = "80"                                  # Listener port
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group.arn # Target group ARN
  }
}

# ECS Service
# Defines an ECS service to run and maintain a specified number of instances of a task definition
resource "aws_ecs_service" "app_service" {
  name            = "app-first-service"
  cluster         = aws_ecs_cluster.streamlit_cluster.id              # ECS Cluster
  task_definition = aws_ecs_task_definition.alcemy_streamlit_task.arn # Task definition
  launch_type     = "FARGATE"
  desired_count   = 3 # Number of task instances to run

  # Load balancer settings for the service
  load_balancer {
    target_group_arn = aws_lb_target_group.target_group.arn # Target group ARN
    container_name   = "alcemy-streamlit-container"         # Container name in the task definition
    container_port   = 8501                                 # Container port to forward traffic to
  }

  # Network configuration for the service
  network_configuration {
    subnets          = [aws_default_subnet.default_subnet_a.id, aws_default_subnet.default_subnet_b.id]
    assign_public_ip = true                                           # Assign public IP to each task
    security_groups  = [aws_security_group.service_security_group.id] # Security group for the service
  }
}

# Security Group for the ECS Service
# Defines a security group for the ECS service with specific ingress and egress rules
resource "aws_security_group" "service_security_group" {
  ingress {
    from_port       = 0 # Allow all inbound traffic from the ALB security group
    to_port         = 0
    protocol        = "-1"
    security_groups = [aws_security_group.load_balancer_security_group.id]
  }

  egress {
    from_port   = 0 # Allow all outbound traffic
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
