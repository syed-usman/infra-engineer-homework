variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "us-west-2"
}

variable "availability_zone_a" {
  description = "The availability zone to use."
  default     = "us-west-2a"
}


variable "availability_zone_b" {
  description = "The availability zone to use."
  default     = "us-west-2b"
}

variable "ecr_repo_uri" {
  description = "The ECR URI+tag of image"
  default     = "408998009634.dkr.ecr.us-west-2.amazonaws.com/alcemy-streamlit-app:latest"
}

variable "gitlab_access_token" {
  description = "access token for gitlab"
  type        = string
}


