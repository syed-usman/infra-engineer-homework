# Output for VPC ID
output "vpc_id" {
  # ID of the default VPC
  value = aws_default_vpc.default_vpc.id
}

# Output for ECS Cluster Name
output "ecs_cluster_name" {
  # name of the ECS cluster created
  value = aws_ecs_cluster.streamlit_cluster.name
}

# Output for Application URL
output "app_url" {
  # DNS name of the Application Load Balancer.
  # This URL is used to access the deployed application.
  value = aws_alb.application_load_balancer.dns_name
}
