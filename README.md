# alcemy Infrastructure Engineer Homework

## Intro

### Overview of the Application Architecture
Our application is a Streamlit app, neatly containerized and hosted on AWS ECS (Elastic Container Service). 

We're leveraging AWS Fargate for serverless operation, which means we don't need to manage the underlying compute instances – AWS handles that for us.

The deployment region is AWS's `us-west-2`, with resources spread across two availability zones `us-west-2a` and `us-west-2b`. This setup ensures high availability; if one zone experiences issues, the other can continue serving the application.

### Setup

This app uses Streamlit, a web app framework for Python. To run it:
- make sure you have a recent version of Python installed. 
- Then use `pip` to install `pipenv`
- Use pipenv to install this app's dependencies like so: `pipenv install`.

### Running

The app can be started simply by calling `streamlit run homework/app.py`. 

This will start the interpreter and open port `8501` which can then be accessed locally.

### Testing

Tests (not yet fully implemented) can be run simply by typing `pytest`.


## Journey of a Request through the Architecture

Let’s trace the path of a user request, from initiation to response:

- **User Initiation**: The user accesses the application via a URL. This URL points to our Application Load Balancer (ALB), which is responsible for directing incoming web traffic to the appropriate destinations.
- **Interaction with ALB:** The ALB resides within the two defined subnets. It receives the request and determines the appropriate container in the ECS cluster to handle it. This decision is facilitated by a target group and a listener on port 80, listening for HTTP requests.
- **Routing to ECS Cluster:** The request is then routed to the appropriate ECS task. Each task represents a running instance of our Streamlit container, ready to process incoming requests.
- **Processing in the Container:** Inside the container, the request is received on Streamlit’s default port 8501. The app processes the request, perhaps querying data or generating output based on the app’s functionality.
- **Response Delivery:** Once processed, the response traverses back through the ECS service to the ALB, and from there, it is delivered to the user's browser.
- **Logging Activity:** Concurrently, all operations are logged via AWS CloudWatch Logs. This is crucial for monitoring the application’s health and troubleshooting if needed.

### Security
Security-wise, we have two distinct security groups: 
- one attached to the ALB, permits inbound traffic on HTTP port 80
- another to the ECS service, which is configured to allow traffic only from the ALB
- IAM roles and security groups were set up with a tight scope. It's like putting each service in its little fortress, just enough room to operate but nothing more.

- Also, we have [SAST](https://docs.gitlab.com/ee/user/application_security/sast/) integrated in the pipeline.


## Choice of AWS Services
- I chose **AWS ECS** and **Fargate** for their serverless nature. That relieves us from managing servers or clusters to deploy a single app. 

- **AWS ECR:** It's defacto for Docker image storage when you're already in the AWS ecosystem.

- **ALB:** The load balancing needs were clear from the start. I needed something to manage incoming traffic

- Simplifying with Defaults
I went with AWS's default VPC and subnets. Could I have customized them or created a new one? Sure, but for the scope of this task, it felt like overkill. 


### Challenges encountered:

- When building the image using Gitlab's shared runner, I tried to install packages using pipenv. That failed because pipenv wanted to start a new thread and Gitlab's runner did not allow it. Then I switched to plain pip and it did not crash anymore. It was probably a knowledge gap regarding the runners on my part and an issue I would explore further on my own.
- Thsi was my forst time working with Terraform directly from GitLab, so there was a learning curve. Also, I believe the current setup still lacks in certain ways:
    - The TF images are pushed and pulled again for different jobs in the pipeline, making the pipeline slow. Could be optimised.
    - I would set up some helper scripts/docs such taht developers can work on their machines using TF state in Gitlab to to make experimentaiton faster. 
    - I would also set up three different enviroments and their pipelines, dev--> stg --> prod.

### Possible Improvements I would make, given more than 6 hours:

- Using a different database (in case that's supposed to grow), and deploying it separately, in a resilient manner e.g. to RDS
- Breaking down terraform structure into separate modules for ECR, ECS, NETWORK, IAM and LB
- [DONE] Using S3/another remote backend for storing terraform state 
- Possibly using Terraform integration in GitLab to have a clean pipeline file
- Switch to a well-thought-out way of testing the container e.g GoogleContainerTools/container-structure-test, instead of just running pytest from the inside. 
- Make a security scan on the container for vulnerabilities as part of the pipeline, before publishing to ECR





