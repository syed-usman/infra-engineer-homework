# Use a specific version of Python as the base image
FROM python:3.11-slim

# Set the working directory in the container
WORKDIR /app

# Install curl for healthcheck and cleanup to reduce image size
RUN apt-get update && \
    apt-get install -y curl && \
    rm -rf /var/lib/apt/lists/*

# Copy the Pipfile and Pipfile.lock into the container
COPY Pipfile Pipfile.lock ./

# Update pip and install pipenv
# Using pip since the Gitlab runner was not able to create a new 
# thread when using pipenv 
RUN pip install --upgrade pip && \
    pip install pipenv

# Generate requirements.txt from Pipfile.lock and install dependencies
RUN pipenv requirements > requirements.txt && \
    pip install -r requirements.txt

# Copy the content of the local source directory to the working directory
COPY . .

# Streamlit runs on port 8501 by default
EXPOSE 8501

# The HEALTHCHECK instruction tells Docker how to test a container to check that it is still working. 
HEALTHCHECK CMD curl --fail http://localhost:8501/_stcore/health

# Command to run the app, replace 'app.py' with the name of your Streamlit application file
CMD ["streamlit", "run", "homework/app.py"]
