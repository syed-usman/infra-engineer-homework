from database.model import engine, Customer, Base

from sqlalchemy.orm import Session

Base.metadata.create_all(engine)

with Session(engine) as session:
    customers = [
        Customer(name="Google",    field="Eyewear"),
        Customer(name="Amazon",    field="Forests"),
        Customer(name="Apple",     field="Fruit"),
        Customer(name="Microsoft", field="Ice Cream")
    ]

    session.add_all(customers)

    session.commit()
